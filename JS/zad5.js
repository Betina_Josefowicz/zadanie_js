var Account = function (constr_balance,constr_currency, constr_number){
    this.balance = constr_balance;
    this.currency = constr_currency;
    this.number = constr_number;
}

var person = (function (){

    var detail = {
        first_name: 'Betina',
        last_name: 'Josefowicz',
    };

    calculateBalance = function(){   

        var suma =0
        for(i=0;i<this.accounts_list.length;i++){
                suma += this.accounts_list[i].balance
            }
        return suma;
    };

    return{
    
        first_name: detail.first_name,
        last_name: detail.last_name,
        accounts_list: [new Account(2000,'zlotych',1), new Account(1000, 'zlotych',2)],
        
        sayHello : function (){

            return 'imie:' + this.first_name + '  nazwisko:' + this.last_name + '  liczba_kont:' + this.accounts_list.length 
            + '  saldo:'+ calculateBalance.call(this);
        },

        addAccount: function(account){

            this.accounts_list.push(account);
        },

        findAccount: function(fnumber){
            return this.accounts_list.find((object) => object.number === fnumber );
        },

        withdraw : function(accountNumber, amount){

            return new Promise((resolve,reject)=>{
                var konto = this.findAccount(accountNumber);
                if (konto != undefined && konto.balance>=amount && amount>=0){
                    konto.balance-=amount;
                    //setTimeout(() => (resolve(`wypłacono ${amount} ${konto.currency} z konta numer: ${accountNumber} </br> aktualny stan konta: ${konto.balance}`)),3000);// intepolacja
                    setTimeout(() => (resolve("wypłacono " + amount + konto.currency + ' z konta numer: ' 
                    + accountNumber + '</br> aktualny stan konta: ' + konto.balance)),3000);
                }
                else{
                    reject("Konto o podanym numerze nie istnieje lub podana kwota przekracza ilość dostepnych srodkow na koncie");
                }
            })

        }

    }
})();
    
    console.log(person.sayHello());
    var konto1 = new Account(1200,'zlotych',3);
    var konto2 = new Account(10000,'zlotych',4);
    person.addAccount(konto1);
    person.addAccount(konto2);
    console.log(person.sayHello()); 


      
        person.withdraw(3,100)
        .then(
        (success) => (console.log(success))
        )
        .catch(
        (error) => (console.log(error))
        )

        person.withdraw(4,11100)
        .then(
        (success) => (console.log(success))
        )
        .catch(
        (error) => (console.log(error))
        )