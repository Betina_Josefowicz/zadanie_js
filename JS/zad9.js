var calosc = (function(){

    var Account = function (constr_balance,constr_currency, constr_number){
        this.balance = constr_balance;
        this.currency = constr_currency;
        this.number = constr_number;
    }
    
    var person = (function (){
    
        var detail = {
            first_name: 'Betina',
            last_name: 'Josefowicz',
        };
    
        calculateBalance = function(){   
    
            var suma =0
            for(i=0;i<this.account_list.length;i++){
                    suma += this.account_list[i].balance;
                }
            return suma;
        };
    
        return{
        
            first_name: detail.first_name,
            last_name: detail.last_name,
            account_list: [new Account(2000,'zlotych',1), new Account(1000, 'zlotych',2)],
            
            sayHello : function (){
    
                return 'imie:' + this.first_name + '  nazwisko:' + this.last_name + '  liczba_kont:' + this.account_list.length 
                + '  saldo:'+ calculateBalance.call(this);
            },
    
            addAccount: function(account){
    
                this.account_list.push(account);
            },
    
            findAccount: function(fnumber){
                return this.account_list.find((object) => object.number === fnumber );
            },
    
            withdraw : function(accountNumber, amount){
                 
                return new Promise((resolve,reject)=>{
                    var konto = this.findAccount(accountNumber);
                    if (konto != undefined && konto.balance>=amount && amount>=0){
                        konto.balance-=amount;
                        setTimeout(() => (resolve("wypłacono " + amount + konto.currency + ' z konta numer: ' 
                        + accountNumber + '</br> aktualny stan konta: ' + konto.balance)),3000);
                    }
                    else{
                        reject("Konto o podanym numerze nie istnieje lub podana kwota jest niepoprawna");
                        }
                })
            }
        }
    })();
    
        console.log(person.sayHello()); 
        var konto1 = new Account(1200,'zlotych',3);
        var konto2 = new Account(10000,'zlotych',4);
        person.addAccount(konto1);
        person.addAccount(konto2);
        console.log(person.sayHello()); 
    
        document.getElementById("id_h4").innerHTML = "imię i nazwisko:   " + person.first_name + "   " + person.last_name;
    
        var start = function(){
    
            tresc_diva="";
            for(i=0;i<person.account_list.length;i++){
                tresc_diva=tresc_diva + '<p>' + 'numer konta:' + person.account_list[i].number + '  '+'saldo:' + person.account_list[i].balance + " " + person.account_list[i].currency + '</p>';
               
            }
            document.getElementById("id_p").innerHTML=tresc_diva;    
        };

        var aktywuj_przycisk = function(){
                
            zawartosc_pola_numer_konta = document.getElementById('number').value;
            zawartosc_pola_kwota = document.getElementById('amount').value;
    
            document.getElementById("id_button").disabled = !(zawartosc_pola_numer_konta && zawartosc_pola_kwot);
        };
     
        window.onload = () => {
            start(); 
            aktywuj_przycisk();
        }; 

        var wyplac = function(){
            numer_konta = document.getElementById('number').value;
            wyplata = document.getElementById('amount').value;
            if((isFinite(numer_konta) && isFinite(wyplata)) && numer_konta!=NaN && wyplata!=NaN){
                document.getElementById('kom').innerHTML = 'Trwa przetwarzanie informacji, proszę czekać...'
                numer_konta_p = parseInt(numer_konta);
                wyplata_p = parseInt(wyplata);
                
                person.withdraw(numer_konta_p,wyplata_p)
                .then(
                (success) =>{console.log(success);
                start();
                document.getElementById('kom').innerHTML =success;
                })
                .catch(
                (error) => {(console.log(error));
                document.getElementById('kom').innerHTML =error;
                })
            }
            else {document.getElementById('kom').innerHTML = 'Wprowadzona wartość jest nieprawidłowa';}        
        };
        
        return{
            aktywuj_przycisk_pub: aktywuj_przycisk,
            wyplac_pub: wyplac
        }
        
    })();